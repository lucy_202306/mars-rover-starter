**Daily Report (2023/07/13)**
 **1.Key learning, skills and experience acquired**

  a. Conducted a code review of the assignment, and found that I needed to improve: I did not think well about the boundary value when judging conditions.
 b. Learned unit testing and TDD, understood how to write test code, and became more familiar with TDD through classroom exercises.

**2.Problem / Confusing / Difficulties**

I am not familiar enough with writing unit testings.

 **3.Other Comments / Suggestion**
    Learning about unit testing is fruitful.





# day4-ORID

## O

a. Conducted a code review of the assignment, and found that I needed to improve: I did not think well about the boundary value when judging conditions.
 b. Learned unit testing and TDD, understood how to write test code, and became more familiar with TDD through classroom exercises.

## R

I was fruitful.

## I

why learned TDD? TDD can guarantee the code quality better and specify the function in advance.

## D

Pay attention to the writing of unit test cases, which can ensure high code quality.
