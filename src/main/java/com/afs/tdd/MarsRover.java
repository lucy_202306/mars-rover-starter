package com.afs.tdd;

import java.util.List;

public class MarsRover {
    public Location location;

    public MarsRover(Location location) {
        this.location = location;
    }

    public Location getLocation() {
        return location;
    }

    public void executeCommand(Command command) {
        if (command == Command.MOVE) {
            move(this.location);
        }
        if (command == Command.LEFT) {
            turnLeft(this.location);
        }
        if (command == Command.RIGHT) {
            turnRight(this.location);
        }
    }

    public void move(Location location) {
        switch (location.getDirection()) {
            case North:
                location.setLocationY(location.getLocationY() + 1);
                break;
            case South:
                location.setLocationY(location.getLocationY() - 1);
                break;
            case West:
                location.setLocationX(location.getLocationX() - 1);
                break;
            case East:
                location.setLocationX(location.getLocationX() + 1);
                break;
            default:
                break;
        }
    }

    public void turnLeft(Location location) {
        switch (location.getDirection()) {
            case North:
                location.setDirection(Direction.West);
                break;
            case West:
                location.setDirection(Direction.South);
                break;
            case South:
                location.setDirection(Direction.East);
                break;
            case East:
                location.setDirection(Direction.North);
                break;
            default:
                break;
        }
    }

    public void turnRight(Location location) {
        switch (location.getDirection()) {
            case North:
                location.setDirection(Direction.East);
                break;
            case East:
                location.setDirection(Direction.South);
                break;
            case South:
                location.setDirection(Direction.West);
                break;
            case West:
                location.setDirection(Direction.North);
                break;
            default:
                break;
        }
    }

    public void executeBatchCommand(List<Command> commandList) {
        for (Command command : commandList) {
            executeCommand(command);
        }
    }

}
