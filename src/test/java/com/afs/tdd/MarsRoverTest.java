package com.afs.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MarsRoverTest {
    @Test
    void should_change_location_to_0_1_N_when_executeCommand_given_location_0_0_N_and_move() {
        // given
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.MOVE);

        // then
        Assertions.assertSame(Direction.North, marsRover.getLocation().getDirection());
        Assertions.assertSame(0, marsRover.getLocation().getLocationX());
        Assertions.assertSame(1, marsRover.getLocation().getLocationY());

    }

    @Test
    void should_change_location_to_2_1_S_when_executeCommand_given_location_2_2_S_and_move() {
        // given
        Location location = new Location(2, 2, Direction.South);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.MOVE);

        // then
        Assertions.assertSame(Direction.South, marsRover.getLocation().getDirection());
        Assertions.assertSame(2, marsRover.getLocation().getLocationX());
        Assertions.assertSame(1, marsRover.getLocation().getLocationY());

    }

    @Test
    void should_change_location_to_1_2_W_when_executeCommand_given_location_2_2_W_and_move() {
        // given
        Location location = new Location(2, 2, Direction.West);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.MOVE);

        // then
        Assertions.assertSame(Direction.West, marsRover.getLocation().getDirection());
        Assertions.assertSame(1, marsRover.getLocation().getLocationX());
        Assertions.assertSame(2, marsRover.getLocation().getLocationY());

    }

    @Test
    void should_change_location_to_3_2_E_when_executeCommand_given_location_2_2_E_and_move() {
        // given
        Location location = new Location(2, 2, Direction.East);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.MOVE);

        // then
        Assertions.assertSame(Direction.East, marsRover.getLocation().getDirection());
        Assertions.assertSame(3, marsRover.getLocation().getLocationX());
        Assertions.assertSame(2, marsRover.getLocation().getLocationY());

    }
    @Test
    void should_change_location_to_2_2_W_when_executeCommand_given_location_2_2_N_and_turnLeft() {
        // given
        Location location = new Location(2, 2, Direction.North);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.LEFT);

        // then
        Assertions.assertSame(Direction.West, marsRover.getLocation().getDirection());
        Assertions.assertSame(2, marsRover.getLocation().getLocationX());
        Assertions.assertSame(2, marsRover.getLocation().getLocationY());

    }
    @Test
    void should_change_location_to_2_2_S_when_executeCommand_given_location_2_2_W_and_turnLeft() {
        // given
        Location location = new Location(2, 2, Direction.West);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.LEFT);

        // then
        Assertions.assertSame(Direction.South, marsRover.getLocation().getDirection());
        Assertions.assertSame(2, marsRover.getLocation().getLocationX());
        Assertions.assertSame(2, marsRover.getLocation().getLocationY());

    }
    @Test
    void should_change_location_to_2_2_E_when_executeCommand_given_location_2_2_S_and_turnLeft() {
        // given
        Location location = new Location(2, 2, Direction.South);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.LEFT);

        // then
        Assertions.assertSame(Direction.East, marsRover.getLocation().getDirection());
        Assertions.assertSame(2, marsRover.getLocation().getLocationX());
        Assertions.assertSame(2, marsRover.getLocation().getLocationY());

    }
    @Test
    void should_change_location_to_2_2_N_when_executeCommand_given_location_2_2_E_and_turnLeft() {
        // given
        Location location = new Location(2, 2, Direction.East);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.LEFT);

        // then
        Assertions.assertSame(Direction.North, marsRover.getLocation().getDirection());
        Assertions.assertSame(2, marsRover.getLocation().getLocationX());
        Assertions.assertSame(2, marsRover.getLocation().getLocationY());

    }
    @Test
    void should_change_location_to_2_2_E_when_executeCommand_given_location_2_2_N_and_turnRight() {
        // given
        Location location = new Location(2, 2, Direction.North);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.RIGHT);

        // then
        Assertions.assertSame(Direction.East, marsRover.getLocation().getDirection());
        Assertions.assertSame(2, marsRover.getLocation().getLocationX());
        Assertions.assertSame(2, marsRover.getLocation().getLocationY());

    }
    @Test
    void should_change_location_to_2_2_S_when_executeCommand_given_location_2_2_E_and_turnRight() {
        // given
        Location location = new Location(2, 2, Direction.East);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.RIGHT);

        // then
        Assertions.assertSame(Direction.South, marsRover.getLocation().getDirection());
        Assertions.assertSame(2, marsRover.getLocation().getLocationX());
        Assertions.assertSame(2, marsRover.getLocation().getLocationY());

    }
    @Test
    void should_change_location_to_2_2_W_when_executeCommand_given_location_2_2_S_and_turnRight() {
        // given
        Location location = new Location(2, 2, Direction.South);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.RIGHT);

        // then
        Assertions.assertSame(Direction.West, marsRover.getLocation().getDirection());
        Assertions.assertSame(2, marsRover.getLocation().getLocationX());
        Assertions.assertSame(2, marsRover.getLocation().getLocationY());

    }
    @Test
    void should_change_location_to_2_2_N_when_executeCommand_given_location_2_2_W_and_turnRight() {
        // given
        Location location = new Location(2, 2, Direction.West);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.RIGHT);

        // then
        Assertions.assertSame(Direction.North, marsRover.getLocation().getDirection());
        Assertions.assertSame(2, marsRover.getLocation().getLocationX());
        Assertions.assertSame(2, marsRover.getLocation().getLocationY());

    }

    @Test
    void should_change_location_to_1_1_W_when_executeBatchCommand_given_location_2_2_W_and_turnRight() {
        // given
        Location location = new Location(2, 2, Direction.West);
        MarsRover marsRover = new MarsRover(location);
        List<Command> commandList = new ArrayList<>();
        commandList.add(Command.MOVE);
        commandList.add(Command.LEFT);
        commandList.add(Command.MOVE);
        commandList.add(Command.RIGHT);

        // when
        marsRover.executeBatchCommand(commandList);

        // then
        Assertions.assertSame(Direction.West, marsRover.getLocation().getDirection());
        Assertions.assertSame(1, marsRover.getLocation().getLocationX());
        Assertions.assertSame(1, marsRover.getLocation().getLocationY());

    }
}
